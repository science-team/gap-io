## debian/tests/makecheck.tst -- GAP Test script
## script format: GAP Reference Manual section 7.9 Test Files (GAP 4r8)
##
gap> TestPackageAvailability( "IO" , "=4.9.1" , true );
"/usr/share/gap/pkg/io/"

##
## eos
